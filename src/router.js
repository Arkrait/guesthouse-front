import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Reviews from "./views/Reviews.vue";
import Gallery from "./views/Gallery.vue";
import Contacts from "./views/Contacts.vue";
import About from "./views/About.vue";
import Guarantees from "./views/Guarantees.vue";
import NotFound from "./views/NotFound.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/reviews",
      name: "reviews",
      component: Reviews
    },
    {
      path: "/contacts",
      name: "contacts",
      component: Contacts
    },
    {
      path: "/gallery",
      name: "gallery",
      component: Gallery
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/guarantees",
      name: "guarantees",
      component: Guarantees
    },
    {
      path: "*",
      component: NotFound
    }
  ]
});
