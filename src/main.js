import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import VueI18n from "vue-i18n";

Vue.use(BootstrapVue);
Vue.use(VueI18n);

Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: "ru"
});

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
